# VertexTopologies.jl
The aim of this package is to supply a framework for creating vertex based topologies which one can run models on. It is designed for modelling of biological tissues consisting of cells. It is also designed to play nice with DifferentialEquations.jl.

These topologies are meant to be used only as topologies, while model definition and solution is left to other packages, such as DifferentialEquations.jl.

### Installation
```julia
] dev https://gitlab.com/slcu/teamHJ/niklas/vertextopologies.jl.git
```

### Topology definition
```julia
using VertexTopologies

v1 = Vertex2D(0., 0.)
v2 = Vertex2D(1., 1.)
v3 = Vertex2D(2., 0.)
v4 = Vertex2D(1., -1.)

c1 = Cell(AbstractEdge[], [1., 2.]) ## Uninitialized vector of Edges and concentrations.
c2 = Cell(AbstractEdge[], [3., 4.])
cnull = Cell(AbstractEdge[], [Inf, Inf])

e1 = Edge(v1, v2, c1, cnull)
e2 = Edge(v2, v3, c2, cnull)
e3 = Edge(v3, v4, c2, cnull)
e4 = Edge(v4, v1, c1, cnull)
e5 = Edge(v4, v2, c1, c2)

append!(c1.edges, [e1,e4,e5]) # Link cells to edges.
append!(c2.edges, [e2,e3,e5])

t = Tissue([c1, c2])
```

### Indexing
```julia
t[1] # First cell of the tissue t
t[:] # All cells of the tissue t
t[:, 1] # First concentration of all cells of the tissue t
t[1, :] # All concentrations of cell 1 in the tissue t
t[1, 1:2] # Concentrations 1 to 2 of cell 1 in the tissue t
t[1:2, :] # All concentrations of cells 1 to 2 in the tissue t
t[:, :] # Cell - concentration matrix


c1[1] # first concentration of the cell c1
c1[:] # all concentrations of the cell c1
```

### Visualisation

```julia
using Plots
plot(t, ms=10, lw=8) ## Plot entire tissue
plot!(c1, ms=8, lw=6) ## Plot cell 1
plot!(e1, ms=6, lw=4) ## Plot edge 1
plot!(v1, ms=4) ## Plot vertex 1
```

### Accessing information
```julia
vertices(e1)
vertices(c1)
vertices(t)

edges(c1)
edges(t)

area(c1)

length(e1)

border(c1) # Total length of cell 1's border
border(c1, c2) # length of shared border

neighbours(c1) # All cells which share an edge with cell 1.

concentrations(c1)
```
