mutable struct Vertex2D{T <: Number} <: AbstractVertex{T}
    x::T
    y::T
end


getindex(v::AbstractVertex, ::Colon) = (v.x, v.y)
function getindex(v::AbstractVertex, i::Integer)
    if i == 1
        return v.x
    elseif i == 2
        return v.y
    else
        @error "Invalid index, $i, for vertex"
    end
end

dimension(::Vertex2D) = 2

Base.iterate(v::AbstractVertex, args...) = iterate(v[:], args...)


-(v1::Vertex2D, v2::Vertex2D) = typeof(v1)(v1.x - v2.x, v1.y - v2.y)
+(v1::Vertex2D, v2::Vertex2D) = typeof(v1)(v1.x + v2.x, v1.y + v2.y)
