struct Tissue{T <: AbstractCell} <: AbstractTissue{T}
    cells::Vector{T}
end

# for func in [:getindex, :setindex!]
#     @eval $func(t::AbstractTissue, args) = $func(t.cells, args)
#     @eval $func(t::AbstractTissue, arg, arg2) = $func.(t[arg], arg2)
#     @eval $func(t::AbstractTissue, arg::Union{Colon, AbstractRange}, arg2::Union{Colon, AbstractRange}) = hcat($func.(t[arg], arg2)...)
# end



length(t::AbstractTissue) = sum(length.(t[:]))

for func in [:iterate, :lastindex]
    @eval $func(t::AbstractTissue, args...) = $func(t.cells, args...)
end

Base.similar(t::Tissue) =  Tissue(similar.(t.cells))
Base.similar(t::Tissue, ::Type{T}) where {T<:Number} =  Tissue(similar.(t.cells, T))
