module VertexTopologies

using RecipesBase

import Base: -, +, *, /, getindex, setindex!, length, lastindex, eltype, size

# abstract type AbstractTopologyComponent end
# abstract type AbstractTopologyComponent{T} <: AbstractVector{T} end
# abstract type AbstractTissue{T} <: AbstractTopologyComponent{T} end
# abstract type AbstractVertex{T} <: AbstractTopologyComponent{T} end
# abstract type AbstractEdge{T, T2} <: AbstractTopologyComponent{T} end
# abstract type AbstractCell{T} <: AbstractTopologyComponent{T} end

abstract type AbstractTissue{T} <: AbstractVector{T} end
abstract type AbstractVertex{T} end
abstract type AbstractEdge{T, T2} end
abstract type AbstractCell{T} <: AbstractVector{T} end

Base.eltype(::Type{<:AbstractVertex{T}}) where {T<:Number} = T
Base.eltype(::Type{<:AbstractCell{T}}) where {T<:Number} = T
Base.eltype(::Type{<:AbstractEdge{T}}) where {T<:AbstractVertex} = T
Base.eltype(::Type{<:AbstractTissue{T}}) where {T<:AbstractCell} = T

Base.size(c::AbstractCell) = size(c.concentrations)
Base.size(t::AbstractTissue) = size(t.cells)

export AbstractTissue, AbstractVertex, AbstractEdge, AbstractCell

include("VertexType.jl")
include("EdgeType.jl")
include("CellType.jl")
include("TissueType.jl")
export Vertex2D, Edge, Cell, Tissue

include("PlotRecipes.jl")

include("Accessors.jl")
export vertices, edges, cells, length, area, border, neighbours


end # module
