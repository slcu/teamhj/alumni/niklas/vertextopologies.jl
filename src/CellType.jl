struct Cell{T<:AbstractEdge, T2<:Number} <: AbstractCell{T2}
    edges::Vector{T}
    concentrations::Vector{T2}
    function Cell{T, T2}(e::Vector{T}, c::Vector{T2}) where {T<:AbstractEdge, T2<:Number}
        if length(unique(Iterators.flatten(e))) != length(e)
            error("Error: Cell() - the edges of your cell does not form a closed loop.")
        end
        new(e, c)
    end
end

Cell(edges::Vector{T}, concs::Vector{T2}) where {T<:AbstractEdge, T2<:Number} = Cell{T, T2}(edges, concs)


for func in [:getindex, :setindex!, :iterate, :lastindex, :length]
    @eval $func(c::AbstractCell, args...) = $func(c.concentrations, args...)
end

Base.similar(c::Cell) =  Cell(c.edges, similar(c.concentrations))
Base.similar(c::Cell, ::Type{T}) where {T<:Number} =  Cell(c.edges, similar(c.concentrations, T))
