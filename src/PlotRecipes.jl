@recipe function f(v::AbstractVertex)
    label --> ""
    seriestype := :scatter
    ([v.x], [v.y])
end

# @recipe function f(e::Edge)
@recipe function f(e::AbstractEdge)
    label --> ""
    @series begin
        seriestype := :scatter
        (getfield.(vertices(e), :x), getfield.(vertices(e), :y))
    end
    @series begin
        primary := false
        (getfield.(vertices(e), :x), getfield.(vertices(e), :y))
    end
end


@recipe function f(t::Union{AbstractTissue, AbstractCell})
    label --> ""
    for (i, edge) in enumerate(edges(t))
        @series begin
            primary := i == 1 ? true : false
            (getfield.(vertices(edge), :x), getfield.(vertices(edge), :y))
        end
    end
    for vertex in vertices(t)
        @series begin
            seriestype := :scatter
            primary := false
            vertex
        end
    end
end
