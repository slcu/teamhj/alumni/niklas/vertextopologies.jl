using Pkg
cd(joinpath(@__DIR__(), ".."))
Pkg.activate(".")
using Revise
using VertexTopologies
using Plots
revise()

v1 = Vertex2D(0., 0.)
v2 = Vertex2D(1., 1.)
v3 = Vertex2D(2., 0.)
v4 = Vertex2D(1., -1.)

c1 = Cell(AbstractEdge[], [1., 2.])
c2 = Cell(AbstractEdge[], [3., 4.])
cnull = Cell(AbstractEdge[], [Inf, Inf])

e1 = Edge(v1, v2, c1, cnull)
e2 = Edge(v2, v3, c2, cnull)
e3 = Edge(v3, v4, c2, cnull)
e4 = Edge(v4, v1, c1, cnull)
e5 = Edge(v4, v2, c1, c2)

append!(c1.edges, [e1,e4,e5])
append!(c2.edges, [e2,e3,e5])

t = Tissue([c1, c2])


pyplot()
plot(t,  lw=3, ms=10, legend=true)
plot!(e2, lw=8, fmt=:png)
plot!(c1, fmt=:png, lw=3, ms=10, legend=true)

area(c1)


edges(t)

edges(c1)

v1 - v2

length(e1)
# e1
display(plot(c1; fmt=:png))
area(c1)



border(c1, c2)
border(c2)

neighbours(c1)

c1[2]
revise()
t[:, :]
t[1]
t[1, :]
t[:, 1]

m = [i+j for i=1:3, j=1:4]
1:2 isa AbstractRange
